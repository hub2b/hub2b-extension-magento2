<?php

namespace Hub2b\Marketplace\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Zend_Db_Exception;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @throws Zend_Db_Exception
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $tableName = $setup->getTable('hub2b_posts');

        $setup->getConnection()->dropTable($tableName);

        /** @var Table $table */
        $table = $setup->getConnection()
            ->newTable($tableName)
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                10,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'primary' => true,
                    'nullable' => false,
                ]
            )
            ->addColumn(
                'post_id',
                Table::TYPE_TEXT,
                13,
                [
                    'nullable' => true,
                ]
            )
            ->addColumn(
                'type',
                Table::TYPE_TEXT,
                20,
                [
                    'nullable' => true,
                ],
                'price, stock, product_status, order_status'
            )
            ->addColumn(
                'parent_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true, 'nullable' => false, 'default' => '0'
                ],
                'Parent ID'
            )
            ->addColumn(
                'content',
                Table::TYPE_TEXT,
                null,
                [
                    'nullable' => true,
                ]
            )
            ->addColumn(
                'status',
                Table::TYPE_TEXT,
                2,
                [
                    'nullable' => false,
                    'default' => 'NE',
                ],
                'NE não enviado, ES enviado com sucesso, EF enviado com falha, EE enviado com erros'
            )
            ->addColumn(
                'failures_num',
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => false,
                    'default' => 0,
                ]
            )
            ->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => Table::TIMESTAMP_INIT,
                ]
            )
            ->addColumn(
                'updated_at',
                Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => Table::TIMESTAMP_INIT_UPDATE,
                ]
            )
            ->addColumn(
                'additional_data',
                Table::TYPE_TEXT,
                null,
                [
                    'nullable' => true,
                ]
            )
            ->addIndex(
                $setup->getIdxName($tableName, ['id']),
                ['id']
            )
            ->addIndex(
                $setup->getIdxName($tableName, ['post_id']),
                ['post_id']
            )
            ->addIndex(
                $setup->getIdxName($tableName, ['parent_id']),
                ['parent_id']
            );

        $setup->getConnection()->createTable($table);


		$setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'hub2b_sourceordernumber',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Source Order Number',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'hub2b_sourceordernumber',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Source Order Number',
            ]
        );

		$setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'hub2b_marketplacesite',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Marketplace Site',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'hub2b_marketplacesite',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Marketplace Site',
            ]
        );

		$setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'hub2b_shippingprovider',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Shipping Provider',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'hub2b_shippingprovider',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Shipping Provider',
            ]
        );

		$setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'hub2b_shippingservice',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Shipping Service',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'hub2b_shippingservice',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Shipping Service',
            ]
        );

		$setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'hub2b_installments',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Installments',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'hub2b_installments',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Installments',
            ]
        );

		$setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'hub2b_paymentcode',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Payment Code',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'hub2b_paymentcode',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Payment Code',
            ]
        );
		
        return $this;
    }
}
