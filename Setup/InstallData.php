<?php

namespace Hub2b\Marketplace\Setup;

use Hub2b\Marketplace\Helper\Data;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    /**
     * @var string
     */
    private $table;
    /**
     * @var AdapterInterface
     */
    private $connection;

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $this->table = $setup->getTable('core_config_data');
        $this->connection = $setup->getConnection();

        if ($this->hasToken()) {
            return;
        }

        $this->createToken();
    }

    /**
     * @return bool
     */
    private function hasToken(): bool
    {
        $select = $this->connection
            ->select()
            ->from($this->table)
            ->where('path = ?', Data::CONFIG_TOKEN_API);

        return (bool) $this->connection->fetchRow($select);
    }

    /**
     * @return void
     */
    private function createToken(): void
    {
        $this->connection->insert($this->table, [
            'scope' => 'default',
            'scope_id' => 0,
            'path' => Data::CONFIG_TOKEN_API,
            'value' => $this->generateToken(),
        ]);
    }

    /**
     * @return string
     */
    private function generateToken(): string
    {
        return md5(uniqid(rand(), true));
    }
}
