<?php

namespace Hub2b\Marketplace\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Zend_Db_Exception;

class UpgradeSchema implements UpgradeSchemaInterface
{
	    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
	
		$setup->startSetup();
		
		$setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'hub2b_sourceordernumber',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Source Order Number',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'hub2b_sourceordernumber',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Source Order Number',
            ]
        );

		$setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'hub2b_marketplacesite',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Marketplace Site',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'hub2b_marketplacesite',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Marketplace Site',
            ]
        );

		$setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'hub2b_shippingprovider',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Shipping Provider',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'hub2b_shippingprovider',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Shipping Provider',
            ]
        );

		$setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'hub2b_shippingservice',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Shipping Service',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'hub2b_shippingservice',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Shipping Service',
            ]
        );

		$setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'hub2b_installments',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Installments',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'hub2b_installments',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Installments',
            ]
        );

		$setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'hub2b_paymentcode',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Payment Code',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'hub2b_paymentcode',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Hub2b Payment Code',
            ]
        );

        $setup->endSetup();
    }
}
