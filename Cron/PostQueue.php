<?php

namespace Hub2b\Marketplace\Cron;

use Hub2b\Marketplace\Api\PostRepositoryInterface;
use Hub2b\Marketplace\Model\SendPost;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;

/**
 * Class PostQueue
 * @package Hub2b\Marketplace\Cron
 */
class PostQueue
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var SendPost
     */
    private $sendPost;
    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    /**
     * Constructor
     *
     * @param PostRepositoryInterface $postRepository
     * @param SendPost $sendPost
     * @param LoggerInterface $logger
     */
    public function __construct(
        PostRepositoryInterface $postRepository,
        SendPost $sendPost,
        LoggerInterface $logger
    ) {
        $this->postRepository = $postRepository;
        $this->sendPost = $sendPost;
        $this->logger = $logger;
    }

    public function execute(): void
    {
        $this->logger->addInfo(__('Hub2b Started Cron Job'));

        try {
            $queue = $this->postRepository->getAllPostAvailableToSend();
        } catch (LocalizedException $e) {
            $this->logger->addInfo(__('Hub2b %1', $e->getMessage()));
            return;
        }

        if ($queue->getTotalCount()) {
            $this->sendPost->execute($queue->getItems());
        }

        $this->logger->addInfo(__('Hub2b Finished Cron Job'));

        return;
    }
}
