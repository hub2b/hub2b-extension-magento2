<?php

namespace Hub2b\Marketplace\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Disabled
 * @package Hub2b\Marketplace\Block\Adminhtml\Form\Field
 */
class Disabled extends Field
{
    /**
     * @inheritDoc
     */
    public function render(AbstractElement $element)
    {
        $element->setDisabled(true);
        return parent::render($element);
    }
}
