<?php

namespace Hub2b\Marketplace\Api;

use Hub2b\Marketplace\Api\Data\PostInterface;
use Hub2b\Marketplace\Api\Data\PostSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface PostRepositoryInterface
 * @package Hub2b\Marketplace\Api
 */
interface PostRepositoryInterface
{
    /**
     * @param PostInterface $post
     * @return PostInterface
     * @throws LocalizedException
     */
    public function save(PostInterface $post);

    /**
     * @param string $id
     * @return PostInterface
     * @throws LocalizedException
     */
    public function getById($id);

    /**
     * @param string $postId
     * @return PostInterface
     * @throws LocalizedException
     */
    public function getByPostId($postId);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return PostSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @return PostSearchResultsInterface
     * @throws LocalizedException
     */
    public function getAllPostAvailableToSend();

    /**
     * @param PostInterface $post
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(PostInterface $post);

    /**
     * @param string $postId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($postId);
}
