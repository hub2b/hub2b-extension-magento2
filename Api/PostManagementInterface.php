<?php

namespace Hub2b\Marketplace\Api;

use Hub2b\Marketplace\Api\Data\PostInterface;

/**
 * Interface PostManagementInterface
 * @package Hub2b\Marketplace\Api
 */
interface PostManagementInterface
{
    /**
     * @param string $type
     * @param int $parentId
     * @param array $content
     * @return PostInterface|null
     */
    public function createPost(string $type, int $parentId, array $content = []): ?PostInterface;

    /**
     * @param PostInterface $post
     * @return PostInterface
     */
    public function successfullyConcluded(PostInterface $post): PostInterface;

    /**
     * @param PostInterface $post
     * @return PostInterface
     */
    public function tryWithFail(PostInterface $post): PostInterface;
}
