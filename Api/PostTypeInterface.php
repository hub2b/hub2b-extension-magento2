<?php

namespace Hub2b\Marketplace\Api;

/**
 * Interface PostTypeInterface
 * @package Hub2b\Marketplace\Api
 */
interface PostTypeInterface
{
    /**
     * @return bool
     */
    public function send(): bool;
}
