<?php

namespace Hub2b\Marketplace\Api;

/**
 * Interface CategoryListInterface
 * @package Hub2b\Marketplace\Api
 */
interface CategoryListInterface extends \Magento\Catalog\Api\CategoryListInterface
{
}
