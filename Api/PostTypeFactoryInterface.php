<?php

namespace Hub2b\Marketplace\Api;

use Hub2b\Marketplace\Api\Data\PostInterface;

/**
 * Interface PostTypeFactoryInterface
 * @package Hub2b\Marketplace\Api
 */
interface PostTypeFactoryInterface
{
    /**
     * @param PostInterface $post
     * @return PostTypeInterface
     */
    public function create(PostInterface $post): PostTypeInterface;
}
