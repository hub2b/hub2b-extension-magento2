<?php

namespace Hub2b\Marketplace\Api\Data;

/**
 * Interface AddressInterface
 * @package Hub2b\Marketplace\Api\Data
 */
interface AddressInterface extends CustomerInterface
{
    const STREET = 'street';
    const NUMBER = 'number';
    const DISTRICT = 'district';
    const COMPLEMENT = 'complement';
	const REFERENCE = 'reference';
    const CITY = 'city';
    const REGION = 'region';
    const COUNTRY = 'country_id';
    const POSTCODE = 'postcode';

    /**
     * @return string
     */
    public function getStreet(): string;

    /**
     * @param string $street
     */
    public function setStreet(string $street): void;

    /**
     * @return string|null
     */
    public function getNumber(): ?string;

    /**
     * @param string $number
     */
    public function setNumber(string $number): void;

    /**
     * @return string
     */
    public function getDistrict(): string;

    /**
     * @param string $district
     */
    public function setDistrict(string $district): void;

    /**
     * @return string|null
     */
    public function getComplement(): ?string;

    /**
     * @param string $complement
     */
    public function setComplement(string $complement): void;

    /**
     * @return string|null
     */
    public function getReference(): ?string;

    /**
     * @param string $complement
     */
    public function setReference(string $reference): void;

    /**
     * @return string
     */
    public function getCity(): string;

    /**
     * @param string $city
     */
    public function setCity(string $city): void;

    /**
     * @return string
     */
    public function getRegion(): string;

    /**
     * @param string $region
     */
    public function setRegion(string $region): void;

    /**
     * @return string
     */
    public function getCountry(): string;

    /**
     * @param string $country
     */
    public function setCountry(string $country): void;

    /**
     * @return string
     */
    public function getPostcode(): string;

    /**
     * @param string $postcode
     */
    public function setPostcode(string $postcode): void;

    /**
     * @return array
     */
    public function toArray(): array;
}
