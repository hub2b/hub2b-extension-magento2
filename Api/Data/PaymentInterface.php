<?php

namespace Hub2b\Marketplace\Api\Data;

/**
 * Interface PaymentInterface
 * @package Hub2b\Marketplace\Api\Data
 */
interface PaymentInterface
{
    const METHOD = 'method';
    const DESCRIPTION = 'description';
	const INSTALLMENTS = 'installments';
	const MAPPEDCODE = 'mappedcode';	

    /**
     * @return string
     */
    public function getMethod(): string;

    /**
     * @param string $method
     */
    public function setMethod(string $method): void;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @param string $price
     */
    public function setDescription(string $price): void;

    /**
     * @return \Hub2b\Marketplace\Api\Data\AddressInterface
     */
    public function getAddress(): \Hub2b\Marketplace\Api\Data\AddressInterface;

    /**
     * @param \Hub2b\Marketplace\Api\Data\AddressInterface $address
     */
    public function setAddress(\Hub2b\Marketplace\Api\Data\AddressInterface $address): void;
	
	    /**
     * @return string
     */
    public function getInstallments(): string;

    /**
     * @param string $installments
     */
    public function setInstallments(string $installments): void;
	
	    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @param string $installments
     */
    public function setCode(string $code): void;	
}
