<?php

namespace Hub2b\Marketplace\Api\Data;

/**
 * Interface ItemInterface
 * @package Hub2b\Marketplace\Api\Data
 */
interface ItemInterface
{
    const SKU = 'sku';
    const QUANTITY = 'quantity';
    const PRICE = 'price';
    const DISCOUNT = 'discount';

    /**
     * @return string
     */
    public function getSku(): string;

    /**
     * @param string $sku
     */
    public function setSku(string $sku): void;

    /**
     * @return int
     */
    public function getQuantity(): int;

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void;

    /**
     * @return float
     */
    public function getPrice(): float;

    /**
     * @param float $price
     */
    public function setPrice(float $price): void;

    /**
     * @return float|null
     */
    public function getDiscount(): ?float;

    /**
     * @param float $discount
     */
    public function setDiscount(float $discount): void;

    /**
     * @return float
     */
    public function getFinalPrice(): float;
}
