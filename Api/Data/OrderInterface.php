<?php

namespace Hub2b\Marketplace\Api\Data;

/**
 * Interface OrderInterface
 * @package Hub2b\Marketplace\Api\Data
 */
interface OrderInterface
{
    const ORDER_ID = 'orderId';
    const SEQUENCE = 'sequence';
    const MARKETPLACE_SITE = 'marketplaceSite';
    const ITEMS = 'items';
    const SHIPPING = 'shipping';
    const PAYMENT = 'payment';
	const HUB2B_SOURCEORDERNUMBER = 'hub2b_sourceordernumber';
	const HUB2B_MARKETPLACESITE = 'hub2b_marketplacesite';
	const HUB2B_SHIPPINGPROVIDER = 'hub2b_shippingprovider';
	const HUB2B_SHIPPINGSERVICE = 'hub2b_shippingservice';
	const HUB2B_INSTALLMENTS = 'hub2b_installments';
	const HUB2B_PAYMENTCODE = 'hub2b_paymentcode';

    /**
     * @return string
     */
    public function getOrderId(): string;

    /**
     * @param string $orderId
     */
    public function setOrderId(string $orderId): void;

    /**
     * @return int
     */
    public function getSequence(): int;

    /**
     * @param int $sequence
     */
    public function setSequence(int $sequence): void;

    /**
     * @return string
     */
    public function getMarketplaceSite(): string;

    /**
     * @param string $marketplaceSite
     */
    public function setMarketplaceSite(string $marketplaceSite): void;

    /**
     * @return \Hub2b\Marketplace\Api\Data\ItemInterface[]
     */
    public function getItems(): array;

    /**
     * @param array $items
     */
    public function setItems(array $items): void;

    /**
     * @return \Hub2b\Marketplace\Api\Data\ShippingInterface
     */
    public function getShipping(): \Hub2b\Marketplace\Api\Data\ShippingInterface;

    /**
     * @param \Hub2b\Marketplace\Api\Data\ShippingInterface $shipping
     */
    public function setShipping(\Hub2b\Marketplace\Api\Data\ShippingInterface $shipping): void;

    /**
     * @return \Hub2b\Marketplace\Api\Data\PaymentInterface
     */
    public function getPayment(): \Hub2b\Marketplace\Api\Data\PaymentInterface;

    /**
     * @param \Hub2b\Marketplace\Api\Data\PaymentInterface $payment
     */
    public function setPayment(\Hub2b\Marketplace\Api\Data\PaymentInterface $payment): void;
	
    public function getHub2bSourceOrderNumber(): string;
    public function setHub2bSourceOrderNumber(string $sourceOrderNumber): void;	
		
    public function getHub2bMarketplaceSite(): string;
    public function setHub2bMarketplaceSite(string $marketplaceSite): void;
		
    public function getHub2bShippingProvider(): string;
    public function setHub2bShippingProvider(string $shippingProvider): void;
		
    public function getHub2bShippingService(): string;
    public function setHub2bShippingService(string $shippingService): void;
		
    public function getHub2bInstallments(): string;
    public function setHub2bInstallments(string $installments): void;
	
    public function getHub2bPaymentCode(): string;
    public function setHub2bPaymentCode(string $paymentCode): void;
}
