<?php

namespace Hub2b\Marketplace\Api\Data;

/**
 * Interface ShippingInterface
 * @package Hub2b\Marketplace\Api\Data
 */
interface ShippingInterface
{
    const METHOD = 'method';
    const PRICE = 'price';
	const SERVICE = 'service';
	const PROVIDER = 'provider';

    /**
     * @return string
     */
    public function getMethod(): string;

    /**
     * @param string $method
     */
    public function setMethod(string $method): void;

    /**
     * @return float
     */
    public function getPrice(): float;

    /**
     * @param float $price
     */
    public function setPrice(float $price): void;

    /**
     * @return \Hub2b\Marketplace\Api\Data\AddressInterface
     */
    public function getAddress(): \Hub2b\Marketplace\Api\Data\AddressInterface;

    /**
     * @param \Hub2b\Marketplace\Api\Data\AddressInterface $address
     */
    public function setAddress(\Hub2b\Marketplace\Api\Data\AddressInterface $address): void;
	
	    /**
     * @return string
     */
    public function getService(): string;

    /**
     * @param string $method
     */
    public function setService(string $service): void;
	
	    /**
     * @return string
     */
    public function getProvider(): string;

    /**
     * @param string $method
     */
    public function setProvider(string $provider): void;
}
