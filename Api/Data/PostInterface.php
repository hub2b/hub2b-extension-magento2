<?php

namespace Hub2b\Marketplace\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface PostInterface extends ExtensibleDataInterface
{
    const RETRY_LIMIT = 5;

    const TYPE_PRICE = 'price';
    const TYPE_STOCK = 'stock';
    const TYPE_PRODUCT_STATUS = 'product_status';
    const TYPE_ORDER_STATUS = 'order_status';

    const STATUS_NEW = 'NE';
    const STATUS_SUCCESS = 'ES';
    const STATUS_FAIL = 'EF';
    const STATUS_ERROR = 'EE';

    const ID = 'id';
    const POST_ID = 'post_id';
    const TYPE = 'type';
    const PARENT_ID = 'parent_id';
    const CONTENT = 'content';
    const STATUS = 'status';
    const FAILURES_NUM = 'failures_num';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const ADDITIONAL_DATA = 'additional_data';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return PostInterface
     */
    public function setId($id);

    /**
     * @return string|null
     */
    public function getPostId();

    /**
     * @param string $postId
     * @return PostInterface
     */
    public function setPostId($postId);

    /**
     * @return string
     */
    public function getType();

    /**
     * @param string $type
     * @return PostInterface
     */
    public function setType($type);

    /**
     * @return int
     */
    public function getParentId();

    /**
     * @param int $parentId
     * @return PostInterface
     */
    public function setParentId($parentId);

    /**
     * @return array
     */
    public function getContent();

    /**
     * @param $content
     * @return PostInterface
     */
    public function setContent($content);

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @param string $status
     * @return PostInterface
     */
    public function setStatus($status);

    /**
     * @return integer
     */
    public function getFailuresNum();

    /**
     * @param integer $failuresNum
     * @return PostInterface
     */
    public function setFailuresNum($failuresNum);

    /**
     * @return string|null
     */
    public function getAdditionalData();

    /**
     * @param string $additionalData
     * @return PostInterface
     */
    public function setAdditionalData($additionalData);
}
