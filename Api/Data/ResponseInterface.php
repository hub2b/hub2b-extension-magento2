<?php

namespace Hub2b\Marketplace\Api\Data;

/**
 * Interface ResponseInterface
 * @package Hub2b\Marketplace\Api\Data
 */
interface ResponseInterface
{
    /**
     * @param bool $error
     * @return ResponseInterface
     */
    public function setError(bool $error): ResponseInterface;

    /**
     * @param int $code
     * @return ResponseInterface
     */
    public function setCode(int $code): ResponseInterface;

    /**
     * @return bool
     */
    public function requestWasSuccessful(): bool;
}
