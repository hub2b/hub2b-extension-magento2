<?php

namespace Hub2b\Marketplace\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface PostSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return PostInterface[]
     */
    public function getItems();

    /**
     * @param PostInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
