<?php

namespace Hub2b\Marketplace\Api\Data;

/**
 * Interface CustomerInterface
 * @package Hub2b\Marketplace\Api\Data
 */
interface CustomerInterface
{
    const FIRST_NAME = 'firstname';
    const LASTNAME = 'lastname';
    const COMPANY = 'company';
    const EMAIL = 'email';
    const DOCUMENT = 'taxvat';
    const TELEPHONE = 'telephone';

    /**
     * @return string
     */
    public function getFirstname(): string;

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void;

    /**
     * @return string
     */
    public function getLastname(): string;

    /**
     * @param string $lastname
     */
    public function setLastname(string $lastname): void;

    /**
     * @return string|null
     */
    public function getCompany(): ?string;

    /**
     * @param string $company
     */
    public function setCompany(string $company): void;

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @param string $email
     */
    public function setEmail(string $email): void;

    /**
     * @return string
     */
    public function getDocument(): string;

    /**
     * @param string $document
     */
    public function setDocument(string $document): void;

    /**
     * @return string
     */
    public function getTelephone(): string;

    /**
     * @param string $telephone
     */
    public function setTelephone(string $telephone): void;
}
