<?php

namespace Hub2b\Marketplace\Api;

/**
 * Interface OrderManagementInterface
 * @package Hub2b\Marketplace\Api
 */
interface OrderManagementInterface
{
    const HUB2B_API_ORDER_CREATE = 'hub2b_api_order_create';
    const HUB2B_SHIPPING_PRICE = 'hub2b_shipping_price';

    /**
     * @param \Hub2b\Marketplace\Api\Data\OrderInterface $order
     * @return string
     */
    public function createOrder(
        \Hub2b\Marketplace\Api\Data\OrderInterface $order
    ): string;
}
