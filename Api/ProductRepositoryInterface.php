<?php

namespace Hub2b\Marketplace\Api;

/**
 * Interface ProductRepositoryInterface
 * @package Hub2b\Marketplace\Api
 */
interface ProductRepositoryInterface extends \Magento\Catalog\Api\ProductRepositoryInterface
{
}
