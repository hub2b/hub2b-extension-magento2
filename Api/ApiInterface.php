<?php

namespace Hub2b\Marketplace\Api;

use Hub2b\Marketplace\Api\Data\ResponseInterface;

/**
 * Interface ApiInterface
 * @package Hub2b\Marketplace\Api
 */
interface ApiInterface
{
    /**
     * @param string $endpoint
     * @param array $data
     * @return ResponseInterface
     */
    public function send(string $endpoint, array $data = []): ResponseInterface;
}
