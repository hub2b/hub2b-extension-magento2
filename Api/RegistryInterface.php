<?php

namespace Hub2b\Marketplace\Api;

interface RegistryInterface
{
    /**
     * @param $key
     * @return mixed
     */
    public function registry($key);

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function register($key, $value);

    /**
     * @param $key
     */
    public function unregister($key): void;
}
