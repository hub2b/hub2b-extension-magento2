<?php

namespace Hub2b\Marketplace\Model;

use Hub2b\Marketplace\Api\Data\PostInterface;
use Hub2b\Marketplace\Api\PostTypeFactoryInterface;
use Magento\Framework\Event\ManagerInterface;

/**
 * Class ProcessPostQueue
 * @package Hub2b\Marketplace\Model
 */
class SendPost
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'hub2b_post_send';

    /**
     * @var ManagerInterface
     */
    private $eventManager;
    /**
     * @var PostTypeFactoryInterface
     */
    private $postTypeFactory;

    /**
     * SendPost constructor.
     * @param PostTypeFactoryInterface $postTypeFactory
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        PostTypeFactoryInterface $postTypeFactory,
        ManagerInterface $eventManager
    ) {
        $this->eventManager = $eventManager;
        $this->postTypeFactory = $postTypeFactory;
    }

    /**
     * @param PostInterface[] $queue
     */
    public function execute(array $queue): void
    {
        foreach ($queue as $post) {
            $this->eventManager->dispatch(sprintf('%s_before', $this->_eventPrefix), [
                'post' => $post
            ]);

            $eventSuffix = 'success';

            $postType = $this->postTypeFactory->create($post);
            if (!$postType->send()) {
                $eventSuffix = 'error';
            }

            $this->eventManager->dispatch(sprintf('%s_%s', $this->_eventPrefix, $eventSuffix), [
                'post' => $post
            ]);
        }
    }
}
