<?php

namespace Hub2b\Marketplace\Model\Data;

use Hub2b\Marketplace\Api\Data\PostInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Post extends AbstractSimpleObject implements PostInterface
{
    /**
     * @return int
     */
    public function getId()
    {
        return $this->_get(self::ID);
    }

    /**
     * @param int $id
     * @return $this|PostInterface
     */
    public function setId($id)
    {
        $this->setData(self::ID, $id);

        return $this;
    }

    /**
     * @return string
     */
    public function getPostId()
    {
        return $this->_get(self::POST_ID);
    }

    /**
     * @return string|null
     */
    public function getType()
    {
        return $this->_get(self::TYPE);
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->_get(self::PARENT_ID);
    }

    /**
     * @param int $parentId
     * @return $this|PostInterface
     */
    public function setParentId($parentId)
    {
        $this->setData(self::PARENT_ID, $parentId);

        return $this;
    }

    /**
     * @return array
     */
    public function getContent()
    {
        return $this->_get(self::CONTENT);
    }

    /**
     * @param $content
     * @return PostInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * @return string|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * @param string $status
     * @return PostInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @return string|null
     */
    public function getFailuresNum()
    {
        return $this->_get(self::FAILURES_NUM);
    }

    /**
     * @param string $failuresNum
     * @return PostInterface
     */
    public function setFailuresNum($failuresNum)
    {
        return $this->setData(self::FAILURES_NUM, $failuresNum);
    }

    /**
     * @return string|null
     */
    public function getAdditionalData()
    {
        return $this->_get(self::ADDITIONAL_DATA);
    }

    /**
     * @param string $additionalData
     * @return PostInterface
     */
    public function setAdditionalData($additionalData)
    {
        return $this->setData(self::ADDITIONAL_DATA, $additionalData);
    }

    /**
     * @param string $postId
     * @return PostInterface
     */
    public function setPostId($postId)
    {
        return $this->setData(self::POST_ID, $postId);
    }

    /**
     * @param string $type
     * @return PostInterface
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }
}
