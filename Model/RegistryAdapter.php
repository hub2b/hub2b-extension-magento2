<?php

namespace Hub2b\Marketplace\Model;

use Hub2b\Marketplace\Api\RegistryInterface;
use Magento\Framework\Registry;

/**
 * Class RegistryAdapter
 * @package Hub2b\Marketplace\Model
 */
class RegistryAdapter implements RegistryInterface
{
    /**
     * @var Registry
     */
    private $magentoRegistry;

    /**
     * Registry constructor.
     * @param Registry $registry
     */
    public function __construct(
        Registry $registry
    ) {
        $this->magentoRegistry = $registry;
    }

    /**
     * @inheritDoc
     */
    public function registry($key)
    {
        return $this->magentoRegistry->registry($key);
    }

    /**
     * @inheritDoc
     */
    public function register($key, $value)
    {
        $this->magentoRegistry->register($key, $value);
    }

    /**
     * @inheritDoc
     */
    public function unregister($key): void
    {
        $this->magentoRegistry->unregister($key);
    }
}
