<?php

namespace Hub2b\Marketplace\Model;

use Exception;
use Hub2b\Marketplace\Api\Data\PostInterface;
use Hub2b\Marketplace\Api\Data\PostInterfaceFactory;
use Hub2b\Marketplace\Api\Data\PostSearchResultsInterface;
use Hub2b\Marketplace\Api\Data\PostSearchResultsInterfaceFactory;
use Hub2b\Marketplace\Api\PostRepositoryInterface;
use Hub2b\Marketplace\Model\ResourceModel\Post as ResourcePost;
use Hub2b\Marketplace\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class PostRepository implements PostRepositoryInterface
{
    /**
     * @var PostFactory
     */
    protected $postFactory;

    /**
     * @var PostInterfaceFactory
     */
    protected $dataPostFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var JoinProcessorInterface
     */
    protected $extensionAttributesJoinProcessor;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ResourcePost
     */
    protected $resource;

    /**
     * @var PostSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var PostCollectionFactory
     */
    protected $postCollectionFactory;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @param ResourcePost $resource
     * @param PostFactory $postFactory
     * @param PostInterfaceFactory $dataPostFactory
     * @param PostCollectionFactory $postCollectionFactory
     * @param PostSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        ResourcePost $resource,
        PostFactory $postFactory,
        PostInterfaceFactory $dataPostFactory,
        PostCollectionFactory $postCollectionFactory,
        PostSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->resource = $resource;
        $this->postFactory = $postFactory;
        $this->postCollectionFactory = $postCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPostFactory = $dataPostFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        PostInterface $post
    ) {
        $postData = $this->extensibleDataObjectConverter->toNestedArray(
            $post,
            [],
            PostInterface::class
        );

        $postModel = $this->postFactory->create()->setData($postData);

        try {
            $this->resource->save($postModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__('Hub2b could not save the post: %1', $exception->getMessage()));
        }

        return $postModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($id)
    {
        $post = $this->postFactory->create();
        $this->resource->load($post, $id);
        if (!$post->getId()) {
            throw new NoSuchEntityException(__('Hub2b post with id "%1" does not exist.', $id));
        }

        return $post->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getByPostId($postId)
    {
        $post = $this->postFactory->create();
        $this->resource->load($post, $postId, PostInterface::POST_ID);
        if (!$post->getId()) {
            throw new NoSuchEntityException(__('Hub2b post with postId "%1" does not exist.', $postId));
        }

        return $post->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ) {
        $collection = $this->postCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        PostInterface $post
    ) {
        try {
            $postModel = $this->postFactory->create();
            $this->resource->load($postModel, $post->getPostId());
            $this->resource->delete($postModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__('Hub2b could not delete the Post: %1', $exception->getMessage()));
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($postId)
    {
        return $this->delete($this->getById($postId));
    }

    /**
     * @inheritDoc
     */
    public function getAllPostAvailableToSend()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(PostInterface::STATUS, $this->statusAvailableToProcess(), 'in')
            ->create();

        return $this->getList($searchCriteria);
    }

    /**
     * @return array
     */
    private function statusAvailableToProcess(): array
    {
        return [
            PostInterface::STATUS_NEW,
            PostInterface::STATUS_FAIL
        ];
    }
}
