<?php

namespace Hub2b\Marketplace\Model\Payment;

use Magento\Payment\Model\Method\AbstractMethod;

/**
 * Class Hub2bPayment
 * @package Hub2b\Marketplace\Model\Payment
 */
class Hub2bPayment extends AbstractMethod
{
    const CODE = 'hub2bpayment';

    protected $_code = self::CODE;
    protected $_isOffline = true;
}
