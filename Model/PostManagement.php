<?php

namespace Hub2b\Marketplace\Model;

use Hub2b\Marketplace\Api\Data\PostInterface;
use Hub2b\Marketplace\Api\Data\PostInterfaceFactory;
use Hub2b\Marketplace\Api\PostManagementInterface;
use Hub2b\Marketplace\Api\PostRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;

/**
 * Class PostManagement
 * @package Hub2b\Marketplace\Model
 */
class PostManagement implements PostManagementInterface
{
    /**
     * @var PostInterfaceFactory
     */
    private $postFactory;
    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ProductSaveBefore constructor.
     * @param PostInterfaceFactory $postFactory
     * @param PostRepositoryInterface $postRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        PostInterfaceFactory $postFactory,
        PostRepositoryInterface $postRepository,
        LoggerInterface $logger
    ) {
        $this->postFactory = $postFactory;
        $this->postRepository = $postRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function createPost(string $type, int $parentId, array $content = []): PostInterface
    {
        $createPost = $this->postFactory->create();

        $createPost->setType($type);
        $createPost->setPostId(uniqid());
        $createPost->setParentId($parentId);

        if (!empty($content)) {
            $createPost->setContent(json_encode($content));
        }

        return $this->save($createPost);
    }

    /**
     * @param PostInterface $post
     * @return PostInterface
     */
    public function successfullyConcluded(PostInterface $post): PostInterface
    {
        $post->setStatus(PostInterface::STATUS_SUCCESS);

        return $this->save($post);
    }

    /**
     * @param PostInterface $post
     * @return PostInterface
     */
    public function tryWithFail(PostInterface $post): PostInterface
    {
        if ($post->getFailuresNum() < PostInterface::RETRY_LIMIT) {
            $post->setFailuresNum($post->getFailuresNum() + 1);
            $post->setStatus(PostInterface::STATUS_FAIL);
        } else {
            $post->setStatus(PostInterface::STATUS_ERROR);
        }

        return $this->save($post);
    }

    /**
     * @param PostInterface $post
     * @return PostInterface
     */
    private function save(PostInterface $post): PostInterface
    {
        try {
            $post = $this->postRepository->save($post);
        } catch (LocalizedException $e) {
            $this->logger->critical(__('Hub2b failed to save post'), [
                'exception' => $e->getMessage()
            ]);

            return null;
        }

        return $post;
    }
}
