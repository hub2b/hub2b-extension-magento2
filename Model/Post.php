<?php

namespace Hub2b\Marketplace\Model;

use Hub2b\Marketplace\Api\Data\PostInterface;
use Hub2b\Marketplace\Api\Data\PostInterfaceFactory;
use Hub2b\Marketplace\Model\ResourceModel\Post\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Post extends AbstractModel
{
    /**
     * @var PostInterfaceFactory
     */
    protected $postDataFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var string
     */
    protected $_eventPrefix = 'hub2b_posts';

    /**
     * @param Context $context
     * @param RegistryAdapter $registry
     * @param PostInterfaceFactory $postDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param ResourceModel\Post $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        PostInterfaceFactory $postDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\Post $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->postDataFactory = $postDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
    }

    /**
     * @return PostInterface
     */
    public function getDataModel(): PostInterface
    {
        $postData = $this->getData();

        $postDataObject = $this->postDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $postDataObject,
            $postData,
            PostInterface::class
        );

        return $postDataObject;
    }
}
