<?php

namespace Hub2b\Marketplace\Model\PostType\Adapter;

use Hub2b\Marketplace\Api\PostTypeInterface;
use Hub2b\Marketplace\Model\PostType\AbstractProductPostType;
use Magento\Catalog\Model\Product\Attribute\Source\Status;

/**
 * Class ProductStatusAdapter
 * @package Hub2b\Marketplace\Model\PostType\Adapter
 */
class ProductStatusAdapter extends AbstractProductPostType implements PostTypeInterface
{
    /**
     * @inheritDoc
     */
    protected function getData(): array
    {
        return [
            'id' => $this->post->getPostId(),
            'sku' => $this->product->getSku(),
            'enabled' => (bool) ($this->product->getStatus() == Status::STATUS_ENABLED)
        ];
    }
}
