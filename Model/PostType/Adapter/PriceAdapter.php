<?php

namespace Hub2b\Marketplace\Model\PostType\Adapter;

use Hub2b\Marketplace\Api\PostTypeInterface;
use Hub2b\Marketplace\Model\PostType\AbstractProductPostType;

/**
 * Class PriceAdapter
 * @package Hub2b\Marketplace\Model\PostType\Adapter
 */
class PriceAdapter extends AbstractProductPostType implements PostTypeInterface
{
    /**
     * @inheritDoc
     */
    protected function getData(): array
    {
        $data = [
            'id' => $this->post->getPostId(),
            'sku' => $this->product->getSku(),
            'price' => $this->product->getPrice(),
        ];

        if ($specialPrice = $this->product->getSpecialPrice()) {
            $data = array_merge($data, [
                'special_price' => $specialPrice
            ]);
        }

        return $data;
    }
}
