<?php

namespace Hub2b\Marketplace\Model\PostType\Adapter;

use Hub2b\Marketplace\Api\ApiInterface;
use Hub2b\Marketplace\Api\Data\PostInterface;
use Hub2b\Marketplace\Api\PostTypeInterface;
use Hub2b\Marketplace\Helper\Data;
use Hub2b\Marketplace\Model\PostType\AbstractPostType;
use Magento\Sales\Api\OrderRepositoryInterface;

/**
 * Class OrderStatusAdapter
 * @package Hub2b\Marketplace\Model\PostType\Adapter
 */
class OrderStatusAdapter extends AbstractPostType implements PostTypeInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * OrderStatusAdapter constructor.
     * @param ApiInterface $integrationService
     * @param Data $helperData
     * @param PostInterface $post
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        ApiInterface $integrationService,
        Data $helperData,
        PostInterface $post,
        OrderRepositoryInterface $orderRepository
    ) {
        parent::__construct($integrationService, $helperData, $post);
        $this->orderRepository = $orderRepository;
    }

    /**
     * @inheritDoc
     */
    protected function getData(): array
    {
        $order = $this->orderRepository->get($this->post->getParentId());

        return [
            'id' => $this->post->getPostId(),
            'increment_id' => $order->getIncrementId(),
            'status' => $order->getStatus()
        ];
    }
}
