<?php

namespace Hub2b\Marketplace\Model\PostType\Adapter;

use Hub2b\Marketplace\Api\PostTypeInterface;
use Hub2b\Marketplace\Model\PostType\AbstractProductPostType;

/**
 * Class StockAdapter
 * @package Hub2b\Marketplace\Model\PostType\Adapter
 */
class StockAdapter extends AbstractProductPostType implements PostTypeInterface
{
    /**
     * @inheritDoc
     */
    protected function getData(): array
    {
        $stock = $this->product->getExtensionAttributes()->getStockItem();

        return [
            'id' => $this->post->getPostId(),
            'sku' => $this->product->getSku(),
            'stock' => $stock->getQty(),
            'is_in_stock' => (bool) $stock->getIsInStock()
        ];
    }
}
