<?php

namespace Hub2b\Marketplace\Model\PostType;

use Hub2b\Marketplace\Api\ApiInterface;
use Hub2b\Marketplace\Api\Data\PostInterface;
use Hub2b\Marketplace\Helper\Data;

/**
 * Class AbstractPostType
 * @package Hub2b\Marketplace\Model\PostType
 */
abstract class AbstractPostType
{
    /**
     * @var PostInterface
     */
    protected $post;
    /**
     * @var ApiInterface
     */
    protected $integrationService;
    /**
     * @var Data
     */
    private $helperData;

    /**
     * AbstractPostType constructor.
     * @param ApiInterface $integrationService
     * @param Data $helperData
     * @param PostInterface $post
     */
    public function __construct(
        ApiInterface $integrationService,
        Data $helperData,
        PostInterface $post
    ) {
        $this->integrationService = $integrationService;
        $this->helperData = $helperData;
        $this->post = $post;
    }

    /**
     * @return bool
     */
    public function send(): bool
    {
        $result = $this->integrationService->send($this->getApiEndpoint(), $this->getData());

        return $result->requestWasSuccessful();
    }

    /**
     * @return array
     */
    abstract protected function getData(): array;

    /**
     * @return string
     */
    protected function getClientId(): string
    {
        return $this->helperData->getClientId();
    }

    /**
     * @return string
     */
    protected function getApiEndpoint(): string
    {
        return $this->helperData->getEndpointByPostType(
            $this->post->getType()
        );
    }
}
