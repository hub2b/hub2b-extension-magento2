<?php

namespace Hub2b\Marketplace\Model\PostType;

use Hub2b\Marketplace\Api\Data\PostInterface;
use Hub2b\Marketplace\Api\PostTypeFactoryInterface;
use Hub2b\Marketplace\Api\PostTypeInterface;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class PostTypeFactory
 * @package Hub2b\Marketplace\Model\PostType
 */
class PostTypeFactory implements PostTypeFactoryInterface
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * PostTypeFactory constructor.
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
    }

    /**
     * @inheritDoc
     */
    public function create(PostInterface $post): PostTypeInterface
    {
        $postTypeInstance = $this->objectManager->create(
            $this->getInstanceName($post->getType()),
            [
                'post' => $post
            ]
        );

        return $postTypeInstance;
    }

    /**
     * @param string $type
     * @return string
     */
    private function getInstanceName(string $type): string
    {
        $className = '';

        $parts = explode('_', $type);
        foreach ($parts as $partName) {
            $className .= ucfirst($partName);
        }

        return '\\Hub2b\\Marketplace\\Model\\PostType\\Adapter\\' . $className . 'Adapter';
    }
}
