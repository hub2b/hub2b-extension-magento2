<?php

namespace Hub2b\Marketplace\Model\PostType;

use Hub2b\Marketplace\Api\ApiInterface;
use Hub2b\Marketplace\Api\Data\PostInterface;
use Hub2b\Marketplace\Helper\Data;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface\Proxy as ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

abstract class AbstractProductPostType extends AbstractPostType
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var ProductInterface
     */
    protected $product;

    /**
     * AbstractProductPostType constructor.
     * @param ApiInterface $integrationService
     * @param Data $helperData
     * @param PostInterface $post
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        ApiInterface $integrationService,
        Data $helperData,
        PostInterface $post,
        ProductRepositoryInterface $productRepository
    ) {
        parent::__construct($integrationService, $helperData, $post);
        $this->productRepository = $productRepository;
    }

    /**
     * @return bool
     * @throws NoSuchEntityException
     */
    public function send(): bool
    {
        $this->loadProduct();

        return parent::send();
    }

    /**
     * @return void
     * @throws NoSuchEntityException
     */
    protected function loadProduct(): void
    {
        $this->product = $this->productRepository->getById($this->post->getParentId());
    }
}
