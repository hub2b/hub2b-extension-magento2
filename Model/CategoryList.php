<?php

namespace Hub2b\Marketplace\Model;

use Hub2b\Marketplace\Api\CategoryListInterface;

/**
 * Class CategoryList
 * @package Hub2b\Marketplace\Model
 */
class CategoryList extends \Magento\Catalog\Model\CategoryList implements CategoryListInterface
{
}
