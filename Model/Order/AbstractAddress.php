<?php

namespace Hub2b\Marketplace\Model\Order;

use Hub2b\Marketplace\Api\Data\AddressInterface;
use Magento\Framework\Data\AbstractDataObject;

/**
 * Class AbstractAddress
 * @package Hub2b\Marketplace\Model\Order
 */
class AbstractAddress extends AbstractDataObject
{
    const ADDRESS = 'address';

    /**
     * @inheritDoc
     */
    public function getAddress(): AddressInterface
    {
        return $this->data[self::ADDRESS];
    }

    /**
     * @param AddressInterface $address
     */
    public function setAddress(AddressInterface $address): void
    {
        $this->data[self::ADDRESS] = $address;
    }
}
