<?php

namespace Hub2b\Marketplace\Model\Order;

use Hub2b\Marketplace\Api\Data\CustomerInterface;
use Magento\Framework\Data\AbstractDataObject;

/**
 * Class Customer
 * @package Hub2b\Marketplace\Model\Order
 */
class Customer extends AbstractDataObject implements CustomerInterface
{
    /**
     * @inheritDoc
     */
    public function getFirstname(): string
    {
        return $this->data[self::FIRST_NAME];
    }

    /**
     * @inheritDoc
     */
    public function setFirstname(string $firstname): void
    {
        $this->data[self::FIRST_NAME] = $firstname;
    }

    /**
     * @inheritDoc
     */
    public function getLastname(): string
    {
        return $this->data[self::LASTNAME];
    }

    /**
     * @inheritDoc
     */
    public function setLastname(string $lastname): void
    {
        $this->data[self::LASTNAME] = $lastname;
    }

    /**
     * @inheritDoc
     */
    public function getCompany(): ?string
    {
        return $this->data[self::COMPANY];
    }

    /**
     * @inheritDoc
     */
    public function setCompany(string $company): void
    {
        $this->data[self::COMPANY] = $company;
    }

    /**
     * @inheritDoc
     */
    public function getEmail(): string
    {
        return $this->data[self::EMAIL];
    }

    /**
     * @inheritDoc
     */
    public function setEmail(string $email): void
    {
        $this->data[self::EMAIL] = $email;
    }

    /**
     * @inheritDoc
     */
    public function getDocument(): string
    {
        return $this->data[self::DOCUMENT];
    }

    /**
     * @inheritDoc
     */
    public function setDocument(string $document): void
    {
        $this->data[self::DOCUMENT] = $document;
    }

    /**
     * @inheritDoc
     */
    public function getTelephone(): string
    {
        return $this->data[self::TELEPHONE];
    }

    /**
     * @inheritDoc
     */
    public function setTelephone(string $telephone): void
    {
        $this->data[self::TELEPHONE] = $telephone;
    }
}
