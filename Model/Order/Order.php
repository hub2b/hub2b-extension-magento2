<?php

namespace Hub2b\Marketplace\Model\Order;

use Hub2b\Marketplace\Api\Data\AddressInterface;
use Hub2b\Marketplace\Api\Data\ItemInterface;
use Hub2b\Marketplace\Api\Data\OrderInterface;
use Hub2b\Marketplace\Api\Data\PaymentInterface;
use Hub2b\Marketplace\Api\Data\ShippingInterface;
use Magento\Framework\Data\AbstractDataObject;

/**
 * Class Order
 * @package Hub2b\Marketplace\Model\Order
 */
class Order extends AbstractDataObject implements OrderInterface
{
    /**
     * @inheritDoc
     */
    public function getOrderId(): string
    {
        return $this->data[self::ORDER_ID];
    }

    /**
     * @inheritDoc
     */
    public function setOrderId(string $orderId): void
    {
        $this->data[self::ORDER_ID] = $orderId;
    }

    /**
     * @return int
     */
    public function getSequence(): int
    {
        return $this->data[self::SEQUENCE];
    }

    /**
     * @param int $sequence
     */
    public function setSequence(int $sequence): void
    {
        $this->data[self::SEQUENCE] = $sequence;
    }

    /**
     * @inheritDoc
     */
    public function getMarketplaceSite(): string
    {
        return $this->data[self::MARKETPLACE_SITE];
    }

    /**
     * @inheritDoc
     */
    public function setMarketplaceSite(string $marketplaceSite): void
    {
        $this->data[self::MARKETPLACE_SITE] = $marketplaceSite;
    }

    /**
     * @return ItemInterface[]
     */
    public function getItems(): array
    {
        return $this->data[self::ITEMS];
    }

    /**
     * @inheritDoc
     */
    public function setItems(array $items): void
    {
        $this->data[self::ITEMS] = $items;
    }

    /**
     * @return ShippingInterface
     */
    public function getShipping(): ShippingInterface
    {
        return $this->data[self::SHIPPING];
    }

    /**
     * @param ShippingInterface $shipping
     */
    public function setShipping(ShippingInterface $shipping): void
    {
        $this->data[self::SHIPPING] = $shipping;
    }

    /**
     * @return PaymentInterface
     */
    public function getPayment(): PaymentInterface
    {
        return $this->data[self::PAYMENT];
    }

    /**
     * @param PaymentInterface $payment
     */
    public function setPayment(PaymentInterface $payment): void
    {
        $this->data[self::PAYMENT] = $payment;
    }
	
    public function getHub2bSourceOrderNumber(): string
    {
        return $this->data[self::HUB2B_SOURCEORDERNUMBER];
    }
    public function setHub2bSourceOrderNumber(string $sourceOrderNumber): void
    {
        $this->data[self::HUB2B_SOURCEORDERNUMBER] = $sourceOrderNumber;
    }
	
    public function getHub2bMarketplaceSite(): string
    {
        return $this->data[self::HUB2B_MARKETPLACESITE];
    }
    public function setHub2bMarketplaceSite(string $marketplaceSite): void
    {
        $this->data[self::HUB2B_MARKETPLACESITE] = $marketplaceSite;
    }	
	
    public function getHub2bShippingProvider(): string
    {
        return $this->data[self::HUB2B_SHIPPINGPROVIDER];
    }
    public function setHub2bShippingProvider(string $shippingProvider): void
    {
        $this->data[self::HUB2B_SHIPPINGPROVIDER] = $shippingProvider;
    }	
	
    public function getHub2bShippingService(): string
    {
        return $this->data[self::HUB2B_SHIPPINGSERVICE];
    }
    public function setHub2bShippingService(string $shippingService): void
    {
        $this->data[self::HUB2B_SHIPPINGSERVICE] = $shippingService;
    }	
	
    public function getHub2bInstallments(): string
    {
        return $this->data[self::HUB2B_INSTALLMENTS];
    }
    public function setHub2bInstallments(string $installments): void
    {
        $this->data[self::HUB2B_INSTALLMENTS] = $installments;
    }		
	
    public function getHub2bPaymentCode(): string
    {
        return $this->data[self::HUB2B_PAYMENTCODE];
    }
    public function setHub2bPaymentCode(string $paymentCode): void
    {
        $this->data[self::HUB2B_PAYMENTCODE] = $paymentCode;
    }	
}
