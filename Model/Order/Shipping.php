<?php

namespace Hub2b\Marketplace\Model\Order;

use Hub2b\Marketplace\Api\Data\ShippingInterface;

/**
 * Class Shipping
 * @package Hub2b\Marketplace\Model\Order
 */
class Shipping extends AbstractAddress implements ShippingInterface
{
    /**
     * @inheritDoc
     */
    public function getMethod(): string
    {
        return $this->data[self::METHOD];
    }

    /**
     * @inheritDoc
     */
    public function setMethod(string $method): void
    {
        $this->data[self::METHOD] = $method;
    }

    /**
     * @inheritDoc
     */
    public function getPrice(): float
    {
        return $this->data[self::PRICE];
    }

    /**
     * @inheritDoc
     */
    public function setPrice(float $price): void
    {
        $this->data[self::PRICE] = $price / 100;
    }
	
	    /**
     * @inheritDoc
     */
    public function getService(): string
    {
        return $this->data[self::SERVICE];
    }

    /**
     * @inheritDoc
     */
    public function setService(string $service): void
    {
        $this->data[self::SERVICE] = $service;
    }
	
	    /**
     * @inheritDoc
     */
    public function getProvider(): string
    {
        return $this->data[self::PROVIDER];
    }

    /**
     * @inheritDoc
     */
    public function setProvider(string $provider): void
    {
        $this->data[self::PROVIDER] = $provider;
    }
}
