<?php

namespace Hub2b\Marketplace\Model\Order;

use Hub2b\Marketplace\Api\Data\ItemInterface;
use Magento\Framework\Data\AbstractDataObject;

/**
 * Class Item
 * @package Hub2b\Marketplace\Model\Order
 */
class Item extends AbstractDataObject implements ItemInterface
{
    /**
     * @inheritDoc
     */
    public function getSku(): string
    {
        return $this->data[self::SKU];
    }

    /**
     * @inheritDoc
     */
    public function setSku(string $sku): void
    {
        $this->data[self::SKU] = $sku;
    }

    /**
     * @inheritDoc
     */
    public function getQuantity(): int
    {
        return $this->data[self::QUANTITY];
    }

    /**
     * @inheritDoc
     */
    public function setQuantity(int $quantity): void
    {
        $this->data[self::QUANTITY] = $quantity;
    }

    /**
     * @inheritDoc
     */
    public function getPrice(): float
    {
        return $this->data[self::PRICE];
    }

    /**
     * @inheritDoc
     */
    public function setPrice(float $price): void
    {
        $this->data[self::PRICE] = $price / 100;
    }

    /**
     * @inheritDoc
     */
    public function getDiscount(): ?float
    {
        return $this->data[self::DISCOUNT];
    }

    /**
     * @inheritDoc
     */
    public function setDiscount(float $discount): void
    {
        $this->data[self::DISCOUNT] = $discount / 100;
    }

    /**
     * @return float
     */
    public function getFinalPrice(): float
    {
        return $this->getPrice() - $this->getDiscount();
    }
}
