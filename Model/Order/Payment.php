<?php

namespace Hub2b\Marketplace\Model\Order;

use Hub2b\Marketplace\Api\Data\PaymentInterface;

/**
 * Class Payment
 * @package Hub2b\Marketplace\Model\Order
 */
class Payment extends AbstractAddress implements PaymentInterface
{
    /**
     * @inheritDoc
     */
    public function getMethod(): string
    {
        return $this->data[self::METHOD];
    }

    /**
     * @inheritDoc
     */
    public function setMethod(string $method): void
    {
        $this->data[self::METHOD] = $method;
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        return $this->data[self::DESCRIPTION];
    }

    /**
     * @inheritDoc
     */
    public function setDescription(string $description): void
    {
        $this->data[self::DESCRIPTION] = $description;
    }

    /**
     * @inheritDoc
     */
    public function getInstallments(): string
    {
        return $this->data[self::INSTALLMENTS];
    }

    /**
     * @inheritDoc
     */
    public function setInstallments(string $installments): void
    {
        $this->data[self::INSTALLMENTS] = $installments;
    }	

    /**
     * @inheritDoc
     */
    public function getCode(): string
    {
        return $this->data[self::MAPPEDCODE];
    }

    /**
     * @inheritDoc
     */
    public function setCode(string $code): void
    {
        $this->data[self::MAPPEDCODE] = $code;
    }		
}
