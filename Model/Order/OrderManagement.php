<?php

namespace Hub2b\Marketplace\Model\Order;

use Hub2b\Marketplace\Api\Data\OrderInterface;
use Hub2b\Marketplace\Api\OrderManagementInterface;
use Hub2b\Marketplace\Api\RegistryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\QuoteManagement;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class OrderManagement
 * @package Hub2b\Marketplace\Model\Order
 */
class OrderManagement implements OrderManagementInterface
{
    /**
     * @var OrderInterface
     */
    private $orderDto;
    /**
     * @var StoreInterface
     */
    private $store;
    /**
     * @var int
     */
    private $websiteId;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var CustomerInterfaceFactory
     */
    private $customerFactory;
    /**
     * @var CartManagementInterface
     */
    private $cartManagement;
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var QuoteManagement
     */
    private $quoteManagement;
    /**
     * @var RegistryInterface
     */
    private $registry;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

	protected $_searchCriteriaBuilder;
	
    /**
     * OrderManagement constructor.
     * @param StoreManagerInterface $storeManager
     * @param CartManagementInterface $cartManagement
     * @param CartRepositoryInterface $cartRepository
     * @param ProductRepositoryInterface $productRepository
     * @param QuoteManagement $quoteManagement
     * @param CustomerInterfaceFactory $customerFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param RegistryInterface $registry
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        CartManagementInterface $cartManagement,
        CartRepositoryInterface $cartRepository,
        ProductRepositoryInterface $productRepository,
        QuoteManagement $quoteManagement,
        CustomerInterfaceFactory $customerFactory,
        CustomerRepositoryInterface $customerRepository,
        OrderRepositoryInterface $orderRepository,
        RegistryInterface $registry,
		\Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->storeManager = $storeManager;
        $this->customerRepository = $customerRepository;
        $this->customerFactory = $customerFactory;
        $this->cartManagement = $cartManagement;
        $this->cartRepository = $cartRepository;
        $this->productRepository = $productRepository;
        $this->quoteManagement = $quoteManagement;
        $this->registry = $registry;
        $this->orderRepository = $orderRepository;
		
		$this->_searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @param OrderInterface $order
     * @return string
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function createOrder(
        OrderInterface $order
    ): string {
        $this->orderDto = $order;

        $this->loadStoreAndWebsite();

		//check if already exists
		$orders = $this->getOrderByMarketplaceOrderNumber($this->orderDto->getOrderId());
		if (!empty($orders)) {
			foreach ($orders as $order) {
				return $order->getRealOrderId();
			}
		}


        $this->registry->register(self::HUB2B_API_ORDER_CREATE, true);

        $cartId = $this->cartManagement->createEmptyCart();
        $quote = $this->cartRepository->get($cartId);

        $quote->setStore($this->storeManager->getStore());
        $quote->setCurrency();
        $quote->setInventoryProcessed(false);

        $this->extractAddress($quote);
        $this->extractItems($quote);
        $this->defineShippingMethod($quote);
        $this->definePaymentMethod($quote);


		$quote->setHub2bSourceOrderNumber($this->orderDto->getOrderId());
		$quote->setHub2bMarketplaceSite($this->orderDto->getMarketplaceSite());
		$quote->setHub2bShippingProvider($this->orderDto->getShipping()->getProvider());
		$quote->setHub2bShippingService($this->orderDto->getShipping()->getService());
		$quote->setHub2bInstallments($this->orderDto->getPayment()->getInstallments());
		$quote->setHub2bPaymentCode($this->orderDto->getPayment()->getCode());

		$quote->setData('hub2b_sourceordernumber',$this->orderDto->getOrderId());	
		$quote->setData('hub2b_marketplacesite',$this->orderDto->getMarketplaceSite());	
		$quote->setData('hub2b_shippingprovider',$this->orderDto->getShipping()->getProvider());	
		$quote->setData('hub2b_shippingservice',$this->orderDto->getShipping()->getService());	
		$quote->setData('hub2b_installments',$this->orderDto->getPayment()->getInstallments());	
		$quote->setData('hub2b_paymentcode',$this->orderDto->getPayment()->getCode());	

        $this->cartRepository->save($quote);

        $quote->getPayment()->importData([
            'method' => 'hub2bpayment'
        ]);

        $quote->collectTotals()->save();
		
		
		$this->fixPricesAndTotals($cartId);
		//needs to reload;
		$quote = $this->cartRepository->get($cartId);
		
        $orderData = $this->quoteManagement->submit($quote);
        $orderData->setEmailSent(0);

        $this->addOrderInformation($orderData);

        $this->registry->unregister(self::HUB2B_API_ORDER_CREATE);

        if (!$orderData->getEntityId()) {
            throw new LocalizedException(__('Could not create order'));
        }

		return $orderData->getRealOrderId();
    }

    /**
     * @throws NoSuchEntityException
     */
    private function loadStoreAndWebsite(): void
    {
        $this->store = $this->storeManager->getStore();
        $this->websiteId = $this->storeManager->getStore()->getWebsiteId();
    }

    /**
     * @param CartInterface $quote
     */
    private function extractAddress(CartInterface $quote): void
    {
        $shippingAddress = $this->orderDto->getShipping()->getAddress();
		$billingAddress = $this->orderDto->getPayment()->getAddress();

        $quote->setCustomerIsGuest(true);
        $quote->setCustomerFirstname($billingAddress->getFirstname());
        $quote->setCustomerLastname($billingAddress->getLastname());
        $quote->setCustomerEmail($billingAddress->getEmail());
        $quote->setCustomerTaxvat($billingAddress->getDocument());

        $quote->getShippingAddress()->addData(
            $shippingAddress->toArray()
        );

        $quote->getBillingAddress()->addData(
            $billingAddress->toArray()
        );
    }

    /**
     * @param CartInterface $quote
     * @throws NoSuchEntityException
     */
    private function extractItems(CartInterface $quote): void
    {
        foreach ($this->orderDto->getItems() as $item) {
            $product = $this->productRepository->get($item->getSku());
            $product->setPrice($item->getFinalPrice());
			
            $quote->addProduct($product, $this->makeAddRequest($product, $item->getSku(), intval($item->getQuantity())));
        }
    }
	
	private function makeAddRequest(\Magento\Catalog\Model\Product $product, $sku = null, $qty = 1)
   {
       $data = [
           'product' => $product->getEntityId(),
           'qty' => $qty
       ];

       switch ($product->getTypeId()) {
           case \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE:
               $data = $this->setConfigurableRequestOptions($product, $sku, $data);
               break;
           case \Magento\Bundle\Model\Product\Type::TYPE_CODE:
               $data = $this->setBundleRequestOptions($product, $data);
               break;
       }

       $request = new \Magento\Framework\DataObject();
       $request->setData($data);

       return $request;
   }
   
   private function setConfigurableRequestOptions(\Magento\Catalog\Model\Product $product, $sku, array $data)
   {
       /** @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable $typedProduct */
       $typedProduct = $product->getTypeInstance();

       $childProduct = $this->productRepository->get($sku);
       $productAttributeOptions = $typedProduct->getConfigurableAttributesAsArray($product);

       $superAttributes = [];
       foreach ($productAttributeOptions as $option) {
           $superAttributes[$option['attribute_id']] = $childProduct->getData($option['attribute_code']);
       }

       $data['super_attribute'] = $superAttributes;
       return $data;
   }   
   
   private function setBundleRequestOptions(\Magento\Catalog\Model\Product $product, array $data)
   {
       /** @var \Magento\Bundle\Model\Product\Type $typedProduct */
       $typedProduct = $product->getTypeInstance();

       $selectionCollection = $typedProduct->getSelectionsCollection($typedProduct->getOptionsIds($product), $product);

       $options = [];
       foreach ($selectionCollection as $proselection) {
           $options[$proselection->getOptionId()] = $proselection->getSelectionId();
       }

       $data['bundle_option'] = $options;
       return $data;
   }

    /**
     * @param CartInterface $quote
     */
    private function defineShippingMethod(CartInterface $quote): void
    {
		$this->registry->unregister(self::HUB2B_SHIPPING_PRICE);
        $this->registry->register(self::HUB2B_SHIPPING_PRICE, $this->orderDto->getShipping()->getPrice());

        $shippingAddress = $quote->getShippingAddress();
        $shippingAddress
            ->setCollectShippingRates(true)
            ->collectShippingRates()
            ->setShippingMethod('hub2bshipping_hub2bshipping');
    }

    /**
     * @param CartInterface $quote
     */
    private function definePaymentMethod(CartInterface $quote): void
    {
        $quote->setPaymentMethod('hub2bpayment');
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $orderData
     */
    private function addOrderInformation(\Magento\Sales\Api\Data\OrderInterface $orderData): void
    {
        $order = $this->orderRepository->get($orderData->getEntityId());
        $order->addCommentToStatusHistory($this->orderInformation());
		
		/*
		$order->setHub2bSourceOrderNumber($this->orderDto->getOrderId());
		//$order->addCommentToStatusHistory($order->getHub2bSourceOrderNumber());
		$order->setHub2bMarketplaceSite($this->orderDto->getMarketplaceSite());
		//$order->addCommentToStatusHistory($order->getHub2bMarketplaceSite());
		$order->setHub2bShippingProvider($this->orderDto->getShipping()->getProvider());
		//$order->addCommentToStatusHistory($order->getHub2bShippingProvider());
		$order->setHub2bShippingService($this->orderDto->getShipping()->getService());
		//$order->addCommentToStatusHistory($order->getHub2bShippingService());
		$order->setHub2bInstallments($this->orderDto->getPayment()->getInstallments());
		//$order->addCommentToStatusHistory($order->getHub2bInstallments());
		$order->setHub2bPaymentCode($this->orderDto->getPayment()->getCode());
		//$order->addCommentToStatusHistory($order->getHub2bPaymentCode());
		*/
		$order->setData('hub2b_sourceordernumber',$this->orderDto->getOrderId());	
		$order->setData('hub2b_marketplacesite',$this->orderDto->getMarketplaceSite());	
		$order->setData('hub2b_shippingprovider',$this->orderDto->getShipping()->getProvider());	
		$order->setData('hub2b_shippingservice',$this->orderDto->getShipping()->getService());	
		$order->setData('hub2b_installments',$this->orderDto->getPayment()->getInstallments());	
		$order->setData('hub2b_paymentcode',$this->orderDto->getPayment()->getCode());
		
		
        $this->orderRepository->save($order);
    }

    /**
     * @return string
     */
    private function orderInformation(): string
    {
        return __(
            "Order ID: %1<br>Marketplace: %2<br>Metodo Envio: %3<br>Metodo Pagamento: %4<br>Nome Pagamento: %5<br>Parcelas: %6<br>Transportadora: %7<br>Tipo Envio: %8",
            $this->orderDto->getOrderId(),
            $this->orderDto->getMarketplaceSite(),
            $this->orderDto->getShipping()->getMethod(),
            $this->orderDto->getPayment()->getMethod(),
            $this->orderDto->getPayment()->getDescription(),
			$this->orderDto->getPayment()->getInstallments(),
			$this->orderDto->getShipping()->getProvider(),
			$this->orderDto->getShipping()->getService()
        );
    }
	
	    /**
     * @param CartInterface $quote
     * @throws NoSuchEntityException
     */
    private function fixPricesAndTotals($cartId): void
    {
		$quote = $this->cartRepository->get($cartId);
		
		foreach ($quote->getAllItems() as $quoteItem)
		{
			if ($quoteItem->getProduct()->getTypeId() == \Magento\Bundle\Model\Product\Type::TYPE_CODE) {
                    continue;
            }
			
			if ($parent = $quoteItem->getParentItem()) {
				
				$parents[$parent->getProduct()->getData('sku')] = ($parents[$parent->getProduct()->getData('sku')] ?? 0) + $quoteItem->getQty();
				$used[$parent->getProduct()->getData('sku')] = 0;
			}
		}
		
		$totalDifference = 0;
		
		foreach ($quote->getAllItems() as $quoteItem)
		{
			if ($quoteItem->getProduct()->getTypeId() == \Magento\Bundle\Model\Product\Type::TYPE_CODE) {
                    continue;
            }

			$psku = "";
			$pqty = 1;

			if ($parent = $quoteItem->getParentItem()) {
				$psku = $parent->getProduct()->getData('sku');
				$pqty = $parent->getQty();
			}

			foreach ($this->orderDto->getItems() as $item) {

				if ($item->getSku() == $quoteItem->getProduct()->getData('sku') || $psku == $item->getSku())
				{
					$finalPrice = $item->getFinalPrice();

					if ($psku == $item->getSku())
					{
						$divisor = ($parents[$psku] ?? 1);
						//dealing with decimals:
						
						$tryBack = round($finalPrice / $divisor, 2) * $divisor;
						
						//checa e usa a diferença só se não foi usada ainda
						//diferença entre o recalculado e o correto
						//se a diferença for negativa, tira local e adiciona depois no frete (isso evita problema de diferença negativa caso o frete seja 0)
						$localDiff = 0;
						
						if (($used[$psku] ?? 0) != 1)
						{
							if (($finalPrice - $tryBack) < 0)
							{
								$localDiff = $finalPrice - $tryBack;
								$totalDifference += ((($finalPrice - $tryBack) * -1) * $pqty);
							}
							else
								$totalDifference += (($finalPrice - $tryBack) * $pqty);
							
							$used[$psku] = 1;
						}
						
						$finalPrice = round($finalPrice / $divisor + $localDiff, 2);

					}

					$quoteItem->setCustomPrice($finalPrice);
                    $quoteItem->setPrice($finalPrice);
                    $quoteItem->setOriginalCustomPrice($finalPrice);
                    $quoteItem->getProduct()->setIsSuperMode(true);
                    $quoteItem->save();

					break;
				}
			}
		}

		$freightValue = $this->orderDto->getShipping()->getPrice();

		$this->orderDto->getShipping()->setPrice(($totalDifference + $freightValue) * 100);

		$this->defineShippingMethod($quote);

		$quote->collectTotals()->save();
    }
	
	private function round_up($number, $precision = 2)
	{
		$fig = pow(10, $precision);
		return (ceil($number * $fig) / $fig);
	}

	/**
	* @param string $incrementId
	* @return \Magento\Sales\Api\Data\OrderInterface $order
	*/
	public function getOrderByMarketplaceOrderNumber($orderNumber) {
	  $this->_searchCriteriaBuilder->addFilter('hub2b_sourceordernumber', $orderNumber);
	 
	  $order = $this->orderRepository->getList(
		  $this->_searchCriteriaBuilder->create()
	  );
	 
	  return $order->getItems();
	}	
		
}
