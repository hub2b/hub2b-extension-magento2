<?php

namespace Hub2b\Marketplace\Model\Order;

use Hub2b\Marketplace\Api\Data\AddressInterface;

/**
 * Class Address
 * @package Hub2b\Marketplace\Model\Order
 */
class Address extends Customer implements AddressInterface
{
    /**
     * @inheritDoc
     */
    public function getStreet(): string
    {
        return $this->data[self::STREET];
    }

    /**
     * @inheritDoc
     */
    public function setStreet(string $street): void
    {
        $this->data[self::STREET] = $street;
    }

    /**
     * @inheritDoc
     */
    public function getNumber(): ?string
    {
        return $this->data[self::NUMBER];
    }

    /**
     * @inheritDoc
     */
    public function setNumber(string $number): void
    {
        $this->data[self::NUMBER] = $number;
    }

    /**
     * @inheritDoc
     */
    public function getDistrict(): string
    {
        return $this->data[self::DISTRICT];
    }

    /**
     * @inheritDoc
     */
    public function setDistrict(string $district): void
    {
        $this->data[self::DISTRICT] = $district;
    }

    /**
     * @inheritDoc
     */
    public function getComplement(): ?string
    {
        return $this->data[self::COMPLEMENT];
    }

    /**
     * @inheritDoc
     */
    public function setComplement(string $complement): void
    {
        $this->data[self::COMPLEMENT] = $complement;
    }

    /**
     * @inheritDoc
     */
    public function getReference(): ?string
    {
        return $this->data[self::REFERENCE];
    }

    /**
     * @inheritDoc
     */
    public function setReference(string $reference): void
    {
        $this->data[self::REFERENCE] = $reference;
    }
    /**
     * @inheritDoc
     */
    public function getCity(): string
    {
        return $this->data[self::CITY];
    }

    /**
     * @inheritDoc
     */
    public function setCity(string $city): void
    {
        $this->data[self::CITY] = $city;
    }

    /**
     * @inheritDoc
     */
    public function getRegion(): string
    {
        return $this->data[self::REGION];
    }

    /**
     * @inheritDoc
     */
    public function setRegion(string $region): void
    {
        $this->data[self::REGION] = $region;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->data[self::COUNTRY];
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->data[self::COUNTRY] = $country;
    }

    /**
     * @inheritDoc
     */
    public function getPostcode(): string
    {
        return $this->data[self::POSTCODE];
    }

    /**
     * @inheritDoc
     */
    public function setPostcode(string $postcode): void
    {
        $this->data[self::POSTCODE] = $postcode;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $address = parent::toArray();
        $address[self::STREET] = [
            $this->getStreet(),
            $this->getNumber(),
            $this->getDistrict(),
            $this->getComplement(),
			$this->getReference()
        ];

        return $address;
    }
}
