<?php

namespace Hub2b\Marketplace\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Post extends AbstractDb
{
    protected function _construct(): void
    {
        $this->_init('hub2b_posts', 'id');
    }
}
