<?php

namespace Hub2b\Marketplace\Model\ResourceModel\Post;

use Hub2b\Marketplace\Model\Post;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct(): void
    {
        $this->_init(
            Post::class,
            \Hub2b\Marketplace\Model\ResourceModel\Post::class
        );
    }
}
