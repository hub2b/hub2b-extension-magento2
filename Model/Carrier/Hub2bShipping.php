<?php

namespace Hub2b\Marketplace\Model\Carrier;

use Hub2b\Marketplace\Api\RegistryInterface;
use Hub2b\Marketplace\Model\Order\OrderManagement;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Psr\Log\LoggerInterface;

class Hub2bShipping extends AbstractCarrier implements CarrierInterface
{
    const CODE = 'hub2bshipping';

    protected $_code = self::CODE;
    protected $_isFixed = true;

    /**
     * @var ResultFactory
     */
    protected $rateResultFactory;
    /**
     * @var MethodFactory
     */
    protected $rateMethodFactory;
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * Constructor
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param RegistryInterface $registry
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        RegistryInterface $registry,
        array $data = []
    ) {
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->registry = $registry;
    }

    /**
     * {@inheritdoc}
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->isNewOrderByHub2b()) {
            return false;
        }

        $price = $this->registry->registry(OrderManagement::HUB2B_SHIPPING_PRICE);

        $method = $this->rateMethodFactory->create();
        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setMethod($this->_code);
        $method->setPrice($price ?? '0.00');
        $method->setCost($price ?? '0.00');

        $result = $this->rateResultFactory->create();
        $result->append($method);

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }

    /**
     * @return bool
     */
    private function isNewOrderByHub2b(): bool
    {
        return (bool) $this->registry->registry(OrderManagement::HUB2B_API_ORDER_CREATE);
    }
}
