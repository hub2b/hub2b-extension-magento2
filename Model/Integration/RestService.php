<?php

namespace Hub2b\Marketplace\Model\Integration;

use Hub2b\Marketplace\Api\ApiInterface;
use Hub2b\Marketplace\Api\Data\ResponseInterface;
use Hub2b\Marketplace\Helper\Data;
use Magento\Framework\HTTP\ZendClientFactory;
use Psr\Log\LoggerInterface;
use Zend_Http_Client;
use Zend_Http_Client_Exception;

/**
 * Class RestService
 * @package Hub2b\Marketplace\Model\Integration
 */
class RestService implements ApiInterface
{
    /**
     * @var array
     */
    private $headers = [
        'content-type' => 'application/json'
    ];
    /**
     * @var ZendClientFactory
     */
    private $zendClientFactory;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var Data
     */
    private $helperData;
    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * Api constructor.
     * @param ZendClientFactory $zendClientFactory
     * @param ResponseInterface $response
     * @param Data $helperData
     * @param LoggerInterface $logger
     */
    public function __construct(
        ZendClientFactory $zendClientFactory,
        ResponseInterface $response,
        Data $helperData,
        LoggerInterface $logger
    ) {
        $this->zendClientFactory = $zendClientFactory;
        $this->helperData = $helperData;
        $this->logger = $logger;
        $this->response = $response;
    }

    /**
     * @param array $header
     * @return RestService
     */
    public function addHeader(array $header): RestService
    {
        $this->headers = array_merge($this->headers, $header);
        return $this;
    }

    /**
     * @return array
     */
    private function getHeaders(): array
    {
        array_push($this->headers, [
            'hub2btoken' => $this->helperData->getApiToken()
        ]);
        return $this->headers;
    }

    /**
     * @param string $endpoint
     * @param array $data
     * @return ResponseInterface
     */
    public function send(string $endpoint, array $data = []): ResponseInterface
    {
        $this->logger->info(__('Hub2b Integration Start'), [
            'endpoint' => $endpoint,
            'data' => $data
        ]);

        /** @var RestService $client */
        $client = $this->zendClientFactory->create();

        try {
            $client->setMethod(Zend_Http_Client::POST);
            $client->setRawData($this->prepareData($data));
            $client->setHeaders($this->getHeaders());
            $client->setUri($endpoint);

            $result = $client->request();
        } catch (Zend_Http_Client_Exception $e) {
            $this->logger->debug(__('Hub2b Integration Fail'), [
                'exception' => $e->getMessage()
            ]);

            return $this->response->setError(true);
        }

        $this->logger->info(__('Hub2b Integration Done'), [
            'status' => $result->getStatus(),
            'message' => $result->getMessage(),
            'body' => $result->getBody()
        ]);

        return $this->response->setCode($result->getStatus());
    }

    /**
     * @param array $data
     * @return string
     */
    private function prepareData(array $data): string
    {
        $data = array_merge($data, [
            'client_id' => $this->helperData->getClientId()
        ]);

        return json_encode($data);
    }
}
