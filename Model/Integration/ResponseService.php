<?php

namespace Hub2b\Marketplace\Model\Integration;

use Hub2b\Marketplace\Api\Data\ResponseInterface;

/**
 * Class ResponseService
 * @package Hub2b\Marketplace\Model\Integration
 */
class ResponseService implements ResponseInterface
{
    const MIN_RANGE_HTTP_STATUS_SUCCESS = 200;
    const MAX_RANGE_HTTP_STATUS_SUCCESS = 300;
    /**
     * @var int
     */
    private $code;
    /**
     * @var bool
     */
    private $error = false;

    /**
     * @param bool $error
     * @return ResponseInterface
     */
    public function setError(bool $error): ResponseInterface
    {
        $this->error = $error;
        return $this;
    }

    /**
     * @param int $code
     * @return ResponseInterface
     */
    public function setCode(int $code): ResponseInterface
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function requestWasSuccessful(): bool
    {
        if ($this->error) {
            return false;
        }

        return ($this->code >= self::MIN_RANGE_HTTP_STATUS_SUCCESS
            && $this->code < self::MAX_RANGE_HTTP_STATUS_SUCCESS
        );
    }
}
