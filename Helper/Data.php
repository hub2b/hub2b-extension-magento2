<?php

namespace Hub2b\Marketplace\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Data
 * @package Hub2b\Marketplace\Helper
 */
class Data extends AbstractHelper
{
    const CONFIG_TOKEN_API = 'hub2b/general/token';

    /**
     * @return string
     */
    public function getApiToken(): string
    {
        return $this->scopeConfig->getValue(self::CONFIG_TOKEN_API);
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->scopeConfig->getValue('hub2b/general/client_id');
    }

    /**
     * @param string $postType
     * @return string
     */
    public function getEndpointByPostType(string $postType): string
    {
        return $this->scopeConfig->getValue('hub2b/webhook/' . $postType);
    }
}
