<?php

namespace Hub2b\Marketplace\Observer\Hub2b;

use Hub2b\Marketplace\Api\Data\PostInterface;
use Hub2b\Marketplace\Api\PostManagementInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class PostSendSuccess
 * @package Hub2b\Marketplace\Observer\Hub2b
 */
class PostSendSuccess implements ObserverInterface
{
    /**
     * @var PostManagementInterface
     */
    private $postManagement;

    /**
     * PostSendSuccess constructor.
     * @param PostManagementInterface $postManagement
     */
    public function __construct(
        PostManagementInterface $postManagement
    ) {
        $this->postManagement = $postManagement;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        /** @var PostInterface $post */
        $post = $observer->getData('post');

        $this->postManagement->successfullyConcluded($post);
    }
}
