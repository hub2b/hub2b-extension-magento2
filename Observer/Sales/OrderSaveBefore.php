<?php

namespace Hub2b\Marketplace\Observer\Sales;

use Hub2b\Marketplace\Api\Data\PostInterface;
use Hub2b\Marketplace\Api\PostManagementInterface;
use Hub2b\Marketplace\Api\RegistryInterface;
use Hub2b\Marketplace\Model\Order\OrderManagement;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;

/**
 * Class OrderSaveBefore
 * @package Hub2b\Marketplace\Observer\Sales
 */
class OrderSaveBefore implements ObserverInterface
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @var PostManagementInterface
     */
    private $postManagement;
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * OrderSaveBefore constructor.
     * @param PostManagementInterface $postManagement
     * @param RegistryInterface $registry
     */
    public function __construct(
        PostManagementInterface $postManagement,
        RegistryInterface $registry
    ) {
        $this->postManagement = $postManagement;
        $this->registry = $registry;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        $this->order = $observer->getOrder();

        if ($this->isNewOrderByHub2b() || !$this->order->getId()) {
            return;
        }

        if ($this->changeStatus()) {
            $this->postManagement->createPost(PostInterface::TYPE_ORDER_STATUS, $this->order->getId());
        }
    }

    /**
     * @return bool
     */
    private function changeStatus(): bool
    {
        if ($this->order->getOrigData('status') != $this->order->getStatus()) {
            return true;
        }

        if ($this->order->getOrigData('state') != $this->order->getState()) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function isNewOrderByHub2b(): bool
    {
        return (bool) $this->registry->registry(OrderManagement::HUB2B_API_ORDER_CREATE);
    }
}
