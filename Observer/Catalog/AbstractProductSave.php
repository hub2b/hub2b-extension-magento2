<?php

namespace Hub2b\Marketplace\Observer\Catalog;

use Hub2b\Marketplace\Api\PostManagementInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;

/**
 * Class AbstractProductSave
 * @package Hub2b\Marketplace\Observer\Catalog
 */
abstract class AbstractProductSave
{
    /**
     * @var Product
     */
    protected $product;
    /**
     * @var PostManagementInterface
     */
    protected $postManagement;

    /**
     * ProductSaveBefore constructor.
     * @param PostManagementInterface $postManagement
     */
    public function __construct(
        PostManagementInterface $postManagement
    ) {
        $this->postManagement = $postManagement;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        $this->product = $observer->getProduct();
    }

    /**
     * @param string $type
     * @return void
     */
    protected function createPostType(string $type): void
    {
        $this->postManagement->createPost($type, $this->product->getEntityId());
    }
}
