<?php

namespace Hub2b\Marketplace\Observer\Catalog;

use Hub2b\Marketplace\Api\Data\PostInterface;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class ProductSaveAfter
 * @package Hub2b\Marketplace\Observer\Catalog
 */
class ProductSaveAfter extends AbstractProductSave implements ObserverInterface
{
    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        parent::execute($observer);

        if ($this->hasChangedStock()) {
            $this->createPostType(PostInterface::TYPE_STOCK);
        }
    }

    /**
     * @return bool
     */
    private function hasChangedStock(): bool
    {
        $origProduct = $this->product->getStoredData();
        $origStockItem = $origProduct['quantity_and_stock_status'] ?? null;
        if (!$origStockItem) {
            return false;
        }

        /** @var StockItemInterface $currentStockItem */
        $currentStockItem = $this->product->getExtensionAttributes()->getStockItem();

        if ($origStockItem['is_in_stock'] != $currentStockItem->getIsInStock()) {
            return true;
        }

        if ($origStockItem['qty'] != $currentStockItem->getQty()) {
            return true;
        }

        return false;
    }
}
