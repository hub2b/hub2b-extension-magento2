<?php

namespace Hub2b\Marketplace\Observer\Catalog;

use Hub2b\Marketplace\Api\Data\PostInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class ProductSaveBefore
 * @package Hub2b\Marketplace\Observer\Catalog
 */
class ProductSaveBefore extends AbstractProductSave implements ObserverInterface
{
    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        parent::execute($observer);

        if (!$this->product->getId()) {
            return;
        }

        if ($this->hasPriceChanged()) {
            $this->createPostType(PostInterface::TYPE_PRICE);
        }

        if ($this->hasChangedStatus()) {
            $this->createPostType(PostInterface::TYPE_PRODUCT_STATUS);
        }
    }

    /**
     * @return bool
     */
    private function hasPriceChanged(): bool
    {
        $priceAttributes = ['price', 'special_price'];
        foreach ($priceAttributes as $attribute) {
            $currentPrice = $this->formatNumber($this->product->getData($attribute));
            if ($this->product->getOrigData($attribute) != $currentPrice) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $number
     * @return float
     */
    private function formatNumber($number): float
    {
        if (is_null($number)) {
            return 0.0;
        }
        return (float) str_replace(',', '.', $number);
    }

    /**
     * @return bool
     */
    private function hasChangedStatus(): bool
    {
        return ($this->product->getOrigData('status') != $this->product->getStatus());
    }
}
